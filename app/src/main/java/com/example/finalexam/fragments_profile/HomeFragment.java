package com.example.finalexam.fragments_profile;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.finalexam.R;
import com.example.finalexam.db_utils.ToDoDdHelper;

public class HomeFragment extends Fragment {
    EditText editTextToDoTitle;
    EditText editTextToDoDescription;

    Button buttonAdd;

    ToDoDdHelper toDoDbHelper;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toDoDbHelper = new ToDoDdHelper(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editTextToDoTitle = view.findViewById(R.id.editTextToDoTitle);
        editTextToDoDescription = view.findViewById(R.id.editTextToDoDescription);

        buttonAdd = view.findViewById(R.id.buttonAdd);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateFields()) {
                    addBookToDb();
                }
                else {
                    Toast.makeText(getContext(), "Fields are invalid", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addBookToDb() {
        String title = editTextToDoTitle.getText().toString();
        String author = editTextToDoDescription.getText().toString();

        editTextToDoTitle.setText("");
        editTextToDoDescription.setText("");

        Toast.makeText(getContext(), "Success!", Toast.LENGTH_SHORT).show();
    }

    private boolean validateFields() {
        boolean check = true;

        if(TextUtils.isEmpty(editTextToDoTitle.getText()) ||
                TextUtils.isEmpty(editTextToDoDescription.getText())) {
            check = false;
        }

        return check;
    }
}
