package com.example.finalexam.fragments_profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.finalexam.R;
import com.example.finalexam.db_utils.ToDoDdHelper;
import com.example.finalexam.db_utils.ToDoEntity;

import java.util.List;

public class ToDoFragment extends Fragment {

    ListView listViewToDo;
    ToDoDdHelper toDoDbHelper;

    public ToDoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toDoDbHelper = new ToDoDdHelper(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_todo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listViewToDo = view.findViewById(R.id.listViewToDo);

        List<ToDoEntity> toDoEntities = toDoDbHelper.selectAll();

        ArrayAdapter<ToDoEntity> adapter = new ArrayAdapter<ToDoEntity>(
                getContext(),
                R.layout.support_simple_spinner_dropdown_item,
                toDoEntities);
        listViewToDo.setAdapter(adapter);
    }
}
