package com.example.finalexam.db_utils;

public class ToDoEntity {
    private Integer id;
    private String toDoTitle;
    private String toDoDescription;

    public ToDoEntity() {
    }

    public ToDoEntity(Integer id, String toDoTitle, String toDoDescription) {
        this.id = id;
        this.toDoTitle = toDoTitle;
        this.toDoDescription = toDoDescription;
    }

    public ToDoEntity(String toDoTitle, String toDoDescription) {
        this.toDoTitle = toDoTitle;
        this.toDoDescription = toDoDescription;
    }

    public Integer getId() {
        return id;
    }

    public String getToDoTitle() {
        return toDoTitle;
    }

    public String getToDoDescription() {
        return toDoDescription;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ToDoEntity{" +
                "id=" + id +
                ", toDoTitle='" + toDoTitle + '\'' +
                ", toDoDescription='" + toDoDescription + '\'' +
                '}';
    }

    public void setToDoTitle(String toDoTitle) {
        this.toDoTitle = toDoTitle;
    }

    public void setToDoDescription(String toDoDescription) {
        this.toDoDescription = toDoDescription;
    }
}
