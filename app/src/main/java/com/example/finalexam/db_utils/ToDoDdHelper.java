package com.example.finalexam.db_utils;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ToDoDdHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "DB_NAME";
    private static final int VERSION = 1;

    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + ToDoContract.TABLE_NAME + " ("
            + ToDoContract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ToDoContract.TODOTITLE + " TEXT, "
            + ToDoContract.TODODESCRIPTION + " TEXT)";

    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + ToDoContract.TABLE_NAME;

    public ToDoDdHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    public void insert(String toDoTitle, String toDoDescription) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(ToDoContract.TODOTITLE, toDoTitle);
        contentValues.put(ToDoContract.TODODESCRIPTION, toDoDescription);;

        getWritableDatabase().insert(ToDoContract.TABLE_NAME, null, contentValues);
    }

    public List<ToDoEntity> selectAll() {

        String[] projection = new String[]{
                ToDoContract.TODOTITLE, ToDoContract.TODODESCRIPTION,
        };

        String where = ToDoContract.ID + " > 0";
        String[] args = new String[]{};

        String ordering = ToDoContract.ID + " ASC";

        @SuppressLint("Recycle") Cursor cursor = getReadableDatabase().query(
                ToDoContract.TABLE_NAME,
                projection,
                where,
                args,
                null,
                null,
                ordering
        );

        List<ToDoEntity> books = new ArrayList<>();

        while (cursor.moveToNext()) {
            books.add(new ToDoEntity(
                    cursor.getString(cursor.getColumnIndex(ToDoContract.TODOTITLE)),
                    cursor.getString(cursor.getColumnIndex(ToDoContract.TODODESCRIPTION))
            ));
        }

        return books;

    }

}
