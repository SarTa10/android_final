package com.example.finalexam.db_utils;

public class ToDoContract {
    public static final String TABLE_NAME = "TODO";


    public static final String ID = "ID";
    public static final String TODOTITLE = "TITLE";
    public static final String TODODESCRIPTION = "DESCRIPTION";
}
